FROM alpine:latest

COPY . /app

WORKDIR /app

ARG GIT_REPO $GIT_REPO

ARG GIT_URL $GIT_URL

RUN apk add hugo python3 py3-pip git

RUN pip install -r requirements.txt

RUN git clone --recurse-submodules $GIT_URL $GIT_REPO

WORKDIR /app/src

RUN sh update.sh

EXPOSE 80

CMD gunicorn -b 0.0.0.0:80 --log-file - main:app
