from flask import Flask, request

app = Flask(__name__)

@app.route("/")
def index():

    token = request.headers.get("Authorization")

    if token is None:

        return "Invalid Authorization", 403

    if SHA256.new(token.encode("utf-8")).hexdigest() != os.environ.get("AUTHORIZATION_TOKEN"):

        return "Invalid Authorization", 403

    subprocess.run(["bash", "update.sh"], shell=True)

    return "OK", 200


